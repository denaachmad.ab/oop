<?php
    require('animal.php');
    require('ape.php');
    require('frog.php');

    $sheep = new Animal("shaun");

    echo "Nama : " . $sheep->name ."<br>"; // "shaun"
    echo "Jumlah kaki : " . $sheep->legs ."<br>"; // 2
    echo "Berdarah dingin : " . $sheep->cold_blooded ."<br>";// false
    echo "<br>";

    $ape = new Ape("Kera Sakti");

    echo "Nama : " . $ape->name ."<br>";
    echo "Jumlah kaki : " . $ape->legs ."<br>";
    echo "Berdarah dingin : " . $ape->cold_blooded ."<br>";
    echo $ape->yell() ."<br>";
    echo "<br>";

    $frog = new Frog("Buduk");

    echo "Nama : " . $frog->name ."<br>";
    echo "Jumlah kaki : " . $frog->legs ."<br>";
    echo "Berdarah dingin : " . $frog->cold_blooded ."<br>";
    echo $frog->jump() ."<br>";
    echo "<br>";
?>